#!/bin/bash

cp kubernetes/config.yaml .

docker build -f kubernetes/Dockerfile -t kubernetes . 

#docker run -v /var/run/docker.sock:/var/run/docker.sock -v /data/kubelet:/var/lib/kubelet kubernetes /usr/bin/kubelet --config var/lib/kubelet/config.yaml  
docker run -v /var/run/docker.sock:/var/run/docker.sock kubernetes /usr/bin/kubelet --config var/lib/kubelet/config.yaml 